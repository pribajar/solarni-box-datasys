#ifndef GsmModem_h
#define GsmModem_h

#include <inttypes.h>
#include <Arduino.h>
#include <SoftwareSerial.h>

#define MODEM_POWER_PIN 25
#define MODEM_PWRKEY_PIN 6
#define MODEM_SLEEP_PIN 5

#define modemSerial Serial3

class GsmModem
{
private:
  // per object data
  SoftwareSerial *_debug;
  // private methods
  void sendAT(String cmd = "");
  uint8_t waitResponse(uint16_t timeout, String &data, String OK_Resp = "X");
  uint8_t waitResponse(String OK_Resp);
  uint8_t waitResponse();

public:
  // public methods
  GsmModem();
  GsmModem(SoftwareSerial &debug);
  void begin(long speed);
  bool isGprsConnected();
  void sendData(String server, uint16_t port, String relUrl, String contentType, String data, String state);
  void getData(String server, uint16_t port, String relUrl, String data, String status);
  void setPins();
  void powerOn();
  static void powerOff();
};

#endif