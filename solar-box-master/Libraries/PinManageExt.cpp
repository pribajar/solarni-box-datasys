#include "PinManageExt.h"
#include "Arduino.h"

void PinManageExt::pinModeK(uint8_t pin, uint8_t mode){
  uint8_t bit = 1 << pin;
  if (mode == INPUT) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTK &= ~bit;
    SREG = oldSREG;
  } else if (mode == INPUT_PULLUP) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTK |= bit;
    SREG = oldSREG;
  } else {
    uint8_t oldSREG = SREG;
    cli();
    DDRD |= bit;
    SREG = oldSREG;
  }  
}
void PinManageExt::pinModeD(uint8_t pin, uint8_t mode){
    uint8_t bit = 1 << pin;
  if (mode == INPUT) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTD &= ~bit;
    SREG = oldSREG;
  } else if (mode == INPUT_PULLUP) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTD |= bit;
    SREG = oldSREG;
  } else {
    uint8_t oldSREG = SREG;
    cli();
    DDRD |= bit;
    SREG = oldSREG;
  }
}
void PinManageExt::digitalWriteK(uint8_t pin, uint8_t val){
    uint8_t bit = 1 << pin;
  uint8_t oldSREG = SREG;
  cli();

  if (val == LOW) {
    PORTK &= ~bit;
  } else {
    PORTK |= bit;
  }
  SREG = oldSREG;
}
void PinManageExt::digitalWriteD(uint8_t pin, uint8_t val){
  uint8_t bit = 1 << pin;
  uint8_t oldSREG = SREG;
  cli();

  if (val == LOW) {
    PORTD &= ~bit;
  } else {
    PORTD |= bit;
  }
  SREG = oldSREG;    
}