#ifndef ModbusLock_h
#define ModbusLock_h

#include <inttypes.h>
#include <ModbusMaster.h>
#include <SoftwareSerial.h>

#define modbusSerial Serial1

class ModbusLock
{
private:
  // per object data
  bool readStart;
  bool dataReady;
  ModbusMaster *_master;
  SoftwareSerial *_debug;
  // private methods
  uint8_t startRead(uint8_t addr, uint8_t reg, uint8_t count);

public:
  // objects
  //  public methods
  ModbusLock();
  ModbusLock(SoftwareSerial &debug);
  void begin(long speed);
  uint16_t read(uint8_t addr, uint8_t reg);
  void read(uint8_t addr, uint8_t reg, uint8_t count, uint16_t *data);
  void write(uint8_t addr, uint8_t reg, uint16_t value);
};

#endif