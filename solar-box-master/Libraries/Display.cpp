#include "Display.h"

Display::Display(SoftwareSerial &debug)
{
    _debug = &debug;
}
void Display::begin(long speed)
{
    displaySerial.begin(speed);
}
void Display::setPins()
{
    pinMode(DISPLAY_POWER_PIN, OUTPUT);
}
void Display::powerOn()
{
    digitalWrite(DISPLAY_POWER_PIN, 1);
}
void Display::powerOff()
{
    digitalWrite(DISPLAY_POWER_PIN, 0);
}

void Display::changePage(int pageId)
{
    Serial.print("page " + String(pageId));
    Serial.write(0xff);
    Serial.write(0xff);
    Serial.write(0xff);
}

void Display::visible(String comp, byte state)
{
    Serial.print("vis " + comp + "," + String(state));
    Serial.write(0xff);
    Serial.write(0xff);
    Serial.write(0xff);
}