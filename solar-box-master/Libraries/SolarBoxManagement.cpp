#include "SolarBoxManagement.h"
#include "PinManageExt.h"
#include <Wire.h>

SolarBoxManagement::SolarBoxManagement()
{
#ifdef DEBUG
    SoftwareSerial lDebug(DB_TX_PIN, DB_TX_PIN);
    this->debug = &lDebug;
    BarCodeReader lBcReader(debug[0]);
    this->bcReader = &lBcReader;
    GsmModem lModem(debug[0]);
    this->modem = &lModem;
    ModbusLock lModbus(debug[0]);
    this->modbus = &lModbus;
    Display lDisplay(debug[0]);
    this->display = &lDisplay;
#else
    BarCodeReader bcReader();
    this->bcReader = &bcReader;
    GsmModem modem;
    this->modem = &modem;
    ModbusLock modbus;
    this->modbus = &modbus;
    Display display;
    this->display = &display;
#endif
}

void SolarBoxManagement::begin()
{
    modbus->begin(19200);
    modem->begin(115200);
    bcReader->begin(9600);
}

void SolarBoxManagement::setPins()
{
    pinMode(LED_RED_PIN, OUTPUT);
    pinMode(LED_GREEN_PIN, OUTPUT);
    pinMode(LED_YELLOW_PIN, OUTPUT);
    pinMode(LED_BLUE_PIN, OUTPUT);
    PinManageExt::pinModeD(LOCKBOARDS_POWER_PIN, OUTPUT);
    pinMode(LOCKS_POWER_PIN, OUTPUT);
    pinMode(DISPLAY_POWER_PIN, OUTPUT);
    pinMode(USER_INT_PIN, INPUT);
    modem->setPins();
    bcReader->setPins();

    // All leds off
    digitalWrite(LED_RED_PIN, 0);
    digitalWrite(LED_GREEN_PIN, 1);
    digitalWrite(LED_BLUE_PIN, 1);
    digitalWrite(LED_YELLOW_PIN, 1);
}

void SolarBoxManagement::setTimer()
{
    noInterrupts(); // disable all interrupts

    TCCR1A = 0;
    TCCR1B = 0;

    TCNT1 = TIMER_PRELOAD;  // preload timer 1ms
    TCCR1B |= (1 << CS11);  // 8 prescaler
    TIMSK1 |= (1 << TOIE1); // enable timer overflow interrupt ISR
    interrupts();
}

void SolarBoxManagement::userButton_ISR()
{
    powerOff();
}
void SolarBoxManagement::resetTimer()
{
    idleCounter = 0;
}

bool SolarBoxManagement::isUserMode()
{
    userMode = 0;
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(REG_USER_MODE);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        userMode = Wire.read();
    }
    return userMode == 1 ? true : false;
}

void SolarBoxManagement::processTimer()
{
    TCNT1 = TIMER_PRELOAD; // preload timer
    idleCounter++;
    keepAliveCounter++;
    bcReader->counter();
}

void SolarBoxManagement::keepAlive()
{
    keepAliveCounter = 0;
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(REG_KEEP_ALIVE);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        Wire.read();
    }
}

void SolarBoxManagement::lockBoardPowerOn()
{
    PinManageExt::digitalWriteD(LOCKBOARDS_POWER_PIN, 1);
}
void SolarBoxManagement::lockBoardPowerOff()
{
    PinManageExt::digitalWriteD(LOCKBOARDS_POWER_PIN, 0);
}
void SolarBoxManagement::locksPowerOn()
{
    digitalWrite(LOCKS_POWER_PIN, 1);
}
void SolarBoxManagement::locksPowerOff()
{
    digitalWrite(LOCKS_POWER_PIN, 0);
}

void SolarBoxManagement::powerOffPer()
{
    GsmModem::powerOff();
    BarCodeReader::powerOff();
    Display::powerOff();
    lockBoardPowerOff();
    locksPowerOff();
}

void SolarBoxManagement::powerOff()
{
    powerOffPer();
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(REG_POWER_OFF);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        Wire.read();
    }
    delay(50);
}

void SolarBoxManagement::processLoop()
{
    if (idleCounter >= IDLE_TIME)
    {
        powerOff();
    }

    bcReader->listen();

    if (keepAliveCounter >= HEART_BEAT_INTERVAL)
    {
        keepAlive();
    }
}

uint16_t SolarBoxManagement::readI2C16(uint8_t regAddr)
{
    uint16_t raw = 0;
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(regAddr);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        raw = Wire.read();
    }
    delay(10);
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(regAddr + 1);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        uint16_t hRaw = Wire.read();
        raw |= hRaw << 8;
    }
    return raw;
}

bool SolarBoxManagement::isBattHeating()
{
    uint8_t raw_heat = 0;
    Wire.beginTransmission(PIC_I2C_ADDR);
    Wire.write(REG_HEATING);
    Wire.endTransmission();
    Wire.requestFrom(PIC_I2C_ADDR, 1);
    if (Wire.available() == 1)
    {
        raw_heat = Wire.read();
    }
    if (raw_heat > 0)
    {
        return true;
    }
    return false;
}

Vector SolarBoxManagement::getAccData()
{
    int16_t raw_Xacc = (int16_t)readI2C16(REG_ACC_X_LOW);
    Vector vec;
    vec.XAxis = getAngle((int16_t)readI2C16(REG_ACC_X_LOW));
    vec.YAxis = getAngle((int16_t)readI2C16(REG_ACC_Y_LOW));
    vec.ZAxis = getAngle((int16_t)readI2C16(REG_ACC_Z_LOW));
#ifdef DEBUG
    debug->println(vec.XAxis);
    debug->println(vec.YAxis);
    debug->println(vec.ZAxis);
#endif
    return vec;
}

float SolarBoxManagement::getAngle(int16_t value)
{
    float sinVal = value * .000061f;
    if (abs(sinVal) > 1)
        sinVal = 1;
    return (asin(sinVal) * 180. / 3.14);
}

float SolarBoxManagement::getBattVoltage()
{
    uint16_t raw_volt = readI2C16(REG_BATT_VOLT_LOW);
    return 12.0 * (raw_volt / REF_BATT_VOLTAGE);
}

uint16_t SolarBoxManagement::getBattTemp()
{
    return readI2C16(REG_BATT_TEMP_LOW);
}

char *SolarBoxManagement::getBattTempString()
{
    uint16_t raw_temp = readI2C16(REG_BATT_TEMP_LOW);
    static char temp[7] = "000.00";
    if (raw_temp & 0x8000) // if the temperature is negative
    {
        temp[0] = '-';              // put minus sign (-)
        raw_temp = (~raw_temp) + 1; // change temperature value to positive form
    }

    else
    {
        if ((raw_temp >> 4) >= 100) // if the temperature >= 100 °C
            temp[0] = '1';          // put 1 of hundreds
        else                        // otherwise
            temp[0] = ' ';          // put space ' '
    }

    // put the first two digits ( for tens and ones)
    temp[1] = ((raw_temp >> 4) / 10) % 10 + '0'; // put tens digit
    temp[2] = (raw_temp >> 4) % 10 + '0';        // put ones digit

    // put the 4 fraction digits (digits after the point)
    // why 625?  because we're working with 12-bit resolution (default resolution)
    temp[4] = ((raw_temp & 0x0F) * 625) / 1000 + '0';       // put thousands digit
    temp[5] = (((raw_temp & 0x0F) * 625) / 100) % 10 + '0'; // put hundreds digit
                                                            // temp[6] = (((raw_temp & 0x0F) * 625) / 10 )  % 10 + '0';    // put tens digit
    // temp[7] = ( (raw_temp & 0x0F) * 625) % 10 + '0';            // put ones digit
    return temp;
}

void SolarBoxManagement::setup()
{
    setPins();
    setTimer();
    Wire.begin();
    attachInterrupt(digitalPinToInterrupt(USER_INT_PIN), userButton_ISR, RISING);
    if (isUserMode())
    {
        lockBoardPowerOn();
        locksPowerOn();
    }
    modem->powerOn();
    modem->begin(115200);
    uint32_t startMillis = millis();
    while (!modem->isGprsConnected() && (millis() - startMillis) < 5000)
    {
        delay(1000);
    }

    if (isUserMode())
    {
#ifdef DEBUG
        debug->println("User mode 1");
#endif
        bcReader->begin(9600);
        display->begin(9600);
        nexInit();
        display->changePage(1);
    }
}

void SolarBoxManagement::openLock(uint8_t lockNum){
  if (lockNum > 0) {
    uint8_t addr = ((lockNum - 1) / 8) + 1;
    uint8_t lockNum = ((lockNum - 1) % 8) + 1;
    modbus->write(addr, REG_OPEN_LOCK, lockNum);
  }
}

uint8_t SolarBoxManagement::stateOfLock(uint8_t lockNum){
    if (lockNum > 0) {
        uint8_t addr = ((lockNum - 1) / 8) + 1;
        uint8_t lockNum = ((lockNum - 1) % 8) + 1;
        return modbus->read(addr, lockNum);
  }
}