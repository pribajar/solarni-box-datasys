#ifndef BarCodeReader_h
#define BarCodeReader_h

#include <inttypes.h>
#include <Arduino.h>
#include <SoftwareSerial.h>

#define BC_POWER_PIN A1
#define BC_RX_PIN 11
#define BC_TX_PIN 2
#define RC_TIMEOUT 22

class BarCodeReader
{
private:
  // per object data
  SoftwareSerial *_serial;
  bool _readStart;
  bool _dataReady;
  uint8_t _rcCounter;
  SoftwareSerial *_debug;
  // private methods

public:
  // objects
  char data[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  // public methods
  BarCodeReader();
  BarCodeReader(SoftwareSerial &debug);
  void begin(long speed);
  void counter();
  bool isDataReady();
  void processSerialData();
  void listen();
  void setPins();
  void powerOn();
  static void powerOff();
};

#endif