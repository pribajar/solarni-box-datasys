#ifndef Display_h
#define Display_h

#include <inttypes.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Nextion.h>

#define DISPLAY_POWER_PIN A12

#define displaySerial Serial

class Display
{
private:
    // per object data
    // HardwareSerial *_serial;
    SoftwareSerial *_debug;
    // private methods

public:
    // objects

    // public methods
    Display();
    Display(SoftwareSerial &debug);
    void begin(long speed);
    void setPins();
    void powerOn();
    static void powerOff();
    void changePage(int pageId);
    void visible(String comp, byte state);
};

#endif