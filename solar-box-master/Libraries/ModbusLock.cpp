#include "ModbusLock.h"

ModbusLock::ModbusLock(SoftwareSerial &debug)
{
    _debug = &debug;
}

void ModbusLock::begin(long speed)
{
    modbusSerial.begin(speed);
}

uint8_t ModbusLock::startRead(uint8_t addr, uint8_t reg, uint8_t count)
{
    _master->begin(addr, modbusSerial);
    _master->clearResponseBuffer();
    return _master->readHoldingRegisters(reg, count);
}

uint16_t ModbusLock::read(uint8_t addr, uint8_t reg)
{
    uint8_t result = startRead(addr, reg, 1);
    if (result == _master->ku8MBResponseTimedOut)
        return 0xffff;
    if (result == _master->ku8MBSuccess)
    {
        return _master->getResponseBuffer(0);
    }
    return 0xffff;
}

void ModbusLock::read(uint8_t addr, uint8_t reg, uint8_t count, uint16_t *data)
{
    uint8_t result = startRead(addr, reg, count);
    if (result == _master->ku8MBResponseTimedOut)
        data[0] = 0xffff;
    if (result == _master->ku8MBSuccess)
    {
        count++;
        while (count-- > 0)
        {
            data[count - 1] = _master->getResponseBuffer(count - 1);
        }
    }
}
void ModbusLock::write(uint8_t addr, uint8_t reg, uint16_t value)
{
    _master->begin(addr, modbusSerial);
    _master->clearResponseBuffer();
    _master->writeSingleRegister(reg, value);
}