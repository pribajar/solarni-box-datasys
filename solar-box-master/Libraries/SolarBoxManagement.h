#ifndef SolarBoxManagement_h
#define SolarBoxManagement_h

#include <inttypes.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <DS3231.h>
#include "GsmModem.h"
#include "BarCodeReader.h"
#include "ModbusLock.h"
#include "Display.h"

#define LOCKBOARDS_POWER_PIN 4
#define LOCKS_POWER_PIN 37

#define LED_RED_PIN 13
#define LED_GREEN_PIN A4
#define LED_YELLOW_PIN A2
#define LED_BLUE_PIN A3
#define DISPLAY_POWER_PIN A12
#define USER_INT_PIN 2
#define DB_TX_PIN A9

#define PIC_I2C_ADDR 0x04

// MASTER PIC I2C REGISTERS
#define REG_USER_MODE 100
#define REG_BATT_TEMP_LOW 101
#define REG_BATT_TEMP_HIGH 102
#define REG_POWER_OFF 103
#define REG_HEATING 104
#define REG_BATT_VOLT_LOW 105
#define REG_BATT_VOLT_HIGH 106
#define REG_KEEP_ALIVE 107
#define REG_ACC_X_LOW 108
#define REG_ACC_X_HIGH 109
#define REG_ACC_Y_LOW 110
#define REG_ACC_Y_HIGH 111
#define REG_ACC_Z_LOW 112
#define REG_ACC_Z_HIGH 113

// LOCK PIC MODBUS REGISTERS
#define REG_OPEN_LOCK 100

// referencni hodnota 12V
#define REF_BATT_VOLTAGE 550.0

// timer
#define TIMER_PRELOAD 63535      // refresh 1ms
#define IDLE_TIME 60000          // ms
#define CHECK_LOCK_INTERVAL 300  // ms
#define HEART_BEAT_INTERVAL 1000 // ms

#define DEBUG

struct Vector
{
  float XAxis;
  float YAxis;
  float ZAxis;
};

class SolarBoxManagement
{
private:
  // per object data
  bool boardStarted = false;
  uint16_t idleCounter, keepAliveCounter;
  bool userMode = 0;
  //uint8_t currLockNum;
  // private methods
  // methods
  void begin();
  void setPins();
  static void userButton_ISR();
  void keepAlive();
  float getAngle(int16_t value);
  uint16_t readI2C16(uint8_t regAddr);
  void setTimer();

public:
  // objects
  GsmModem *modem;
  BarCodeReader *bcReader;
  ModbusLock *modbus;
  Display *display;
  SoftwareSerial *debug;
  DS3231 rtc;
  // public methods
  SolarBoxManagement();
  void setup();
  void resetTimer();
  void processTimer();
  void processLoop();
  static void powerOffPer();
  static void powerOff();
  void lockBoardPowerOn();
  static void lockBoardPowerOff();
  void locksPowerOn();
  static void locksPowerOff();
  bool isUserMode();
  bool isBattHeating();
  Vector getAccData();
  float getBattVoltage();
  uint16_t getBattTemp();
  char *getBattTempString();
  void openLock(uint8_t lockNum);
  uint8_t stateOfLock(uint8_t lockNum);
};

#endif