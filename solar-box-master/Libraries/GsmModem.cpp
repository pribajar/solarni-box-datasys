#include "GsmModem.h"
#include "PinManageExt.h"

// Constructor

GsmModem::GsmModem(SoftwareSerial &debug)
{
  _debug = &debug;
}
void GsmModem::sendAT(String cmd)
{
  if (cmd == "")
  {
    modemSerial.println();
  }
  else
  {
    if (cmd == "0x1a")
    {
      modemSerial.write(0x1a);
    }
    else
    {
      modemSerial.println(cmd);
    }
  }
}
uint8_t GsmModem::waitResponse(uint16_t timeout, String &data, String OK_Resp)
{
  uint32_t startMillis = millis();
  do
  {
    while (modemSerial.available())
    {
      int8_t a = modemSerial.read();
      if (a <= 0)
        continue;
      data += static_cast<char>(a);
      if (OK_Resp != "X" && (data.endsWith(OK_Resp) || data.indexOf(OK_Resp) > 0))
      {
        return 1;
      }
    }
  } while ((millis() - startMillis) < timeout);
  return 0;
}
uint8_t GsmModem::waitResponse(String OK_Resp)
{
  String data;
  return waitResponse(1000, data, OK_Resp);
}
uint8_t GsmModem::waitResponse()
{
  String data;
  return waitResponse(1000, data, "OK");
}

void GsmModem::begin(long speed)
{
  modemSerial.begin(speed);
  delay(6000);
}
bool GsmModem::isGprsConnected()
{
  sendAT("AT+CGATT?");
  if (waitResponse("+CGATT: 1") != 1)
  {
    return false;
  }
  return true;
}
void GsmModem::sendData(String server, uint16_t port, String relUrl, String contentType, String data, String state)
{
  sendAT("AT+CHTTPACT=\"" + server + "\"," + String(port));
  String response;
  if (waitResponse(2000, response, "REQUEST") == 1)
  {
    // mySerial.println(response);
    sendAT("POST http://" + server + relUrl + " HTTP/1.1");
    sendAT("Host: " + server);
    sendAT("User-Agent: Box stock");
    sendAT("Content-Type:" + contentType);
    sendAT("Content-Length: " + String(data.length()));
    sendAT();
    sendAT(data);
    sendAT("0x1a");
    sendAT();
    response = "";
    waitResponse(2000, response);
    state = "OK";
  }
  else
  {
    state = "ERROR";
  }
}
void GsmModem::getData(String server, uint16_t port, String relUrl, String response, String state)
{
  sendAT("AT+CHTTPACT=\"" + server + "\"," + String(port));
  if (waitResponse(3000, response, "REQUEST") == 1)
  {
    state = "OK";
    sendAT("GET http://" + server + relUrl + " HTTP/1.1");
    sendAT("Host: " + server);
    sendAT("User-Agent: Box stock");
    sendAT("Content-Length: 0");
    sendAT();
    sendAT("0x1a");
    sendAT();
    response = "";
    waitResponse(2000, response);
  }
  else
  {
    state = "ERROR";
  }
}

void GsmModem::setPins()
{
  pinMode(MODEM_POWER_PIN, OUTPUT);
  PinManageExt::pinModeD(MODEM_PWRKEY_PIN, OUTPUT);
  PinManageExt::pinModeD(MODEM_SLEEP_PIN, OUTPUT);
}

void GsmModem::powerOff()
{
  digitalWrite(MODEM_POWER_PIN, 0);
}

void GsmModem::powerOn()
{
  delay(300);
  PinManageExt::digitalWriteD(MODEM_PWRKEY_PIN, 0);
  PinManageExt::digitalWriteD(MODEM_SLEEP_PIN, 0);
  delay(50);
  digitalWrite(MODEM_POWER_PIN, 1);
  delay(10000);
}