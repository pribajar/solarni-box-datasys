#include "BarCodeReader.h"

BarCodeReader::BarCodeReader()
{
    SoftwareSerial serial(BC_RX_PIN, BC_TX_PIN);
    _serial = &serial;
}

BarCodeReader::BarCodeReader(SoftwareSerial &debug)
{
    _debug = &debug;
    BarCodeReader();
}
void BarCodeReader::begin(long speed)
{
    _serial->begin(speed);
}
void BarCodeReader::counter()
{
    if (_readStart)
    {
        _rcCounter++;
    }
}
void BarCodeReader::processSerialData()
{
    memset(data, 0, 20);
    if (_serial->available())
    {
        _rcCounter = 0;
        int ix = 0;
        while (_serial->available())
        {
            byte bite = _serial->read();
            if (bite != 0xff)
            {
                _serial->println(char(bite));
                data[ix++] = char(bite);
            }
        }
        if (ix > 0)
            _dataReady = true;
    }
}

bool BarCodeReader::isDataReady()
{
    if (_dataReady)
    {
        _dataReady = false;
        return true;
    }
    return false;
}

void BarCodeReader::listen()
{
    _serial->listen();
    if (!_readStart && _serial->available())
    {
        _dataReady = false;
        _rcCounter = 0;
        _readStart = true;
    }
    if (_readStart && _rcCounter > RC_TIMEOUT)
    {
        _readStart = false;
        _rcCounter = 0;
        processSerialData();
    }
}
void BarCodeReader::setPins()
{
    pinMode(BC_POWER_PIN, OUTPUT);
}
void BarCodeReader::powerOn()
{
    digitalWrite(BC_POWER_PIN, 1);
}
void BarCodeReader::powerOff()
{
    digitalWrite(BC_POWER_PIN, 0);
}