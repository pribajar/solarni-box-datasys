#ifndef PinManageExt_h

#define PinManageExt_h

#include <inttypes.h>

class PinManageExt
{
private:
  // per object data
  // private methods

public:
    // public methods
    static void pinModeK(uint8_t pin, uint8_t mode);
    static void pinModeD(uint8_t pin, uint8_t mode);
    static void digitalWriteK(uint8_t pin, uint8_t val);
    static void digitalWriteD(uint8_t pin, uint8_t val);
};

#endif