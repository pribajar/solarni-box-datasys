
#include <Nextion.h>
#include "SolarBoxManagement.h"



//#define QRC_READ_DELAY 2000 //spozdeni v ms po nacteni QR kodu / zamezuje opetovnemu nacteni stejneho kodu / nebo cekani po zapnuti ctecky
#define MAX_LOCK_NUM 64     //maximalni pocet zamku


bool checkLockState = false; 
uint16_t lockStateCounter; 
//bool userMode = 0;
uint8_t currLockNum;//, gprsAttempts;
//String json = "CHECK_PARCEL_BARCODE;{\"CheckParcelBarcode\":{\"Barcode\":\"ET123456789SK\"}}";

SolarBoxManagement solarBoxMan;

// Your GPRS credentials, if any
const char apn[]      = "Internet";
//const char gprsUser[] = "";
//const char gprsPass[] = "";

// Server details
const String server   = "picon.i-development.cz";
const String getResource = "/Box/GetLockId?id=";
const String contentType ="text/plain";
const String postResource = "/Box/SaveBoxState";
const int  port       = 5000;

void setup() {
  solarBoxMan.setup();
  if (!solarBoxMan.isUserMode()) {
    solarBoxMan.debug->println("User mode 0");
    _sendDiagnosticData();
    delay(10000);
    solarBoxMan.powerOff();
  } else {
    _attachDisplayEventMethods();
  }
  /*rtc.setYear(22);
    rtc.setMonth(1);
    rtc.setHour(13);
    rtc.setMinute(3);
    rtc.setSecond(1);*/
}

ISR(TIMER1_OVF_vect)                    // interrupt service routine for overflow
{
  solarBoxMan.processTimer();
  if (checkLockState) {
    lockStateCounter++;
  }
}

char * _convertStringToChar8(String value) {
  static char chRes[8];
  value.toCharArray(chRes,value.length()+1);
  return  chRes;
}

char * _getTextRef20(String text){
  static char chText[20];
  memset(chText,0,sizeof(chText));
  text.toCharArray(chText,sizeof(chText));
  return chText;
}


//Declaration of components
NexText tReadCode = NexText(2, 1, "tReadCode");
NexText tLockState = NexText(5, 3, "tLockState");
NexText tGPRS = NexText(4, 3, "tGPRS");
NexText tTemp = NexText(4, 7, "tTemp");
NexText tVolt = NexText(4, 5, "tVolt");
NexText tHeat = NexText(4, 9, "tHeat");
NexText tXAxis = NexText(4, 11, "tXAxis");
NexText tYAxis = NexText(4, 13, "tYAxis");
NexText tZAxis = NexText(4, 15, "tZAxis");
NexText tLockNum = NexText(3, 13, "tLockNum");
NexButton bQRcode = NexButton(1, 1, "bQRcode");
NexButton bMonitor = NexButton(1, 2, "bMonitor");
NexButton bOpenLock = NexButton(5, 2, "bOpenLock");
NexButton bOpenLockMan = NexButton(3, 11, "bOpenLockMan");
NexButton bBackToQRcode2 = NexButton(3, 14, "bBackToQRcode2");
NexButton bBackToQRcode = NexButton(5, 1, "bBackToQRcode");
NexButton bBackToMain = NexButton(2, 3, "bBackToMain");

/******Declaration of Nextion-Callbacks********/

NexTouch *nex_listen_list[] = {
  &bQRcode,
  &bMonitor,
  &bOpenLock,
  &bOpenLockMan,
  &bBackToQRcode,
  &bBackToQRcode2,
  &bBackToMain,
  NULL
};

void bQRcodePushCallback(void *ptr) {
  //_switchBCReader(1);
  solarBoxMan.bcReader->powerOn();
  solarBoxMan.resetTimer();
};

void _processQRCode() {
  if (currLockNum >  0 && currLockNum <= MAX_LOCK_NUM) {
    _refreshLockState();
  }
}

void bMonitorPushCallback(void *ptr) {
  if (solarBoxMan.modem->isGprsConnected()) {
    tGPRS.setText("Pripojeno");
  } else {
    tGPRS.setText("Nepripojeno");
  }
  tTemp.setText(solarBoxMan.getBattTempString());
  tVolt.setText(_convertStringToChar8(String(solarBoxMan.getBattVoltage())));
  tHeat.setText(solarBoxMan.isBattHeating()?"Zapnuto":"Vypnuto");
  Vector vec = solarBoxMan.getAccData();
  tXAxis.setText(_convertStringToChar8(String(vec.XAxis)));
  tYAxis.setText(_convertStringToChar8(String(vec.YAxis)));
  tZAxis.setText(_convertStringToChar8(String(vec.ZAxis)));
  solarBoxMan.resetTimer();
};

void bOpenLockPushCallback(void *ptr) {
  //_openLock();
  solarBoxMan.openLock(currLockNum);
  checkLockState = true;
  solarBoxMan.resetTimer();
};

void bBackToQRcodePushCallback(void *ptr) {
  _goBackToQRcode();
};


void bBackToMainPushCallback(void *ptr) {
  //_switchBCReader(0);
  solarBoxMan.bcReader->powerOff();
  solarBoxMan.resetTimer();
};

void bOpenLockManPushCallback(void *ptr) {
  char chNum[3] = {0, 0, 0};
  tLockNum.getText(chNum, 2);
  uint8_t num =  atoi(chNum);
  tLockNum.setText("");
  if (num > 0 && num <= MAX_LOCK_NUM) {
    solarBoxMan.debug->println("Odemykani zamku " + String(num));
    currLockNum = num;
    solarBoxMan.openLock(currLockNum);
  }
  solarBoxMan.resetTimer();
}

void _attachDisplayEventMethods() {
    bQRcode.attachPush(bQRcodePushCallback);
    bMonitor.attachPush(bMonitorPushCallback);
    bOpenLock.attachPush(bOpenLockPushCallback);
    bOpenLockMan.attachPush(bOpenLockManPushCallback);
    bBackToQRcode.attachPush(bBackToQRcodePushCallback);
    bBackToQRcode2.attachPush(bBackToQRcodePushCallback);
    bBackToMain.attachPush(bBackToMainPushCallback);
}

void _qrProgressText(char * text) {
  tReadCode.setText(text);
}

void _refreshLockState() {
  String text = "Schranka cislo " + String(currLockNum) + " je " + _checkLockState();
  tLockState.setText(text.c_str());
}

String _checkLockState() {
  String res = "Error";
  switch (solarBoxMan.stateOfLock(currLockNum)) {
    case 1:
      {
        res = "zamcena";
        solarBoxMan.display->visible("bOpenLock", 1);
        break;
      }
    case 0:
      {
        res = "odemcena";
        solarBoxMan.display->visible("bOpenLock", 0);
        break;
      }
    case 0xff:
      {
        res = "nedostupna";
        solarBoxMan.display->visible("bOpenLock", 0);
        break;
      }
  }
  return res;
}

void _goBackToQRcode() {
  solarBoxMan.bcReader->powerOn();
  tReadCode.setText("Nactete carovy kod");
  solarBoxMan.resetTimer();
}

void _listenButtonLoop() {
  nexLoop(nex_listen_list);
}

void _sendDiagnosticData() {
  solarBoxMan.debug->println("Send data");
  if (solarBoxMan.modem->isGprsConnected()) {
    solarBoxMan.debug->println("Read diagnostic data");
    String volt = String(solarBoxMan.getBattVoltage());
    delay(10);
    String temp = solarBoxMan.getBattTempString();
    delay(10);
    String heat = solarBoxMan.isBattHeating()?"Zapnuto":"Vypnuto";
    Vector vec = solarBoxMan.getAccData();
    char xAxis[8];
    memset(xAxis,0,sizeof(xAxis));
    String(vec.XAxis).toCharArray(xAxis, sizeof(xAxis));
    
    char yAxis[8];
    memset(yAxis,0,sizeof(yAxis));
    String(vec.YAxis).toCharArray(yAxis, sizeof(yAxis));
    
    char zAxis[8];
    memset(zAxis,0,sizeof(zAxis));
    String(vec.ZAxis).toCharArray(zAxis, sizeof(zAxis));
    
    String data = volt + ";" + temp + ";" + heat + ";" + xAxis + ";" + yAxis + ";" + zAxis;
    solarBoxMan.debug->println(data);
    String state = "";
    solarBoxMan.modem->sendData(server, port, postResource, "text/plain", data, state);
  } else {
    solarBoxMan.debug->println("No data connection");
  }
}

void _lockNumRequest() {
  //sendAT("AT+CHTTPACT=\"" + server + "\"," + String(port));
  String response;
  String id = solarBoxMan.bcReader->data;
  String state;
  solarBoxMan.modem->getData(server, port, getResource + id, response, state);
  solarBoxMan.debug->println(state);
  //if (state == "OK") {
    int ix = 0;
    char num[3] = {0, 0, 0};
    for (int i = 0; i < response.length(); i++) {
      if (ix < 17 && response[i] == '\n') {
        ix++;
      }
      if (ix == 17) {
        num[0] = response[i + 1];
        num[1] = response[i + 2];
        ix++;
        break;
      }
    }
    solarBoxMan.debug->println(num);
    uint8_t iNum = atoi(num);
    if (iNum == 99) {
        solarBoxMan.bcReader->powerOff();
        solarBoxMan.display->changePage(3);
    } else {
      if (iNum > 0 && iNum <= MAX_LOCK_NUM) {
        currLockNum = iNum;
        solarBoxMan.display->changePage(5);
        solarBoxMan.bcReader->powerOff();
        _processQRCode();
      } else {
        _qrProgressText(_getTextRef20(String("Chyba nacitani dat")));
      }
    }
    solarBoxMan.debug->println(response);
}

void loop() {

  solarBoxMan.processLoop();
  
  if (checkLockState && lockStateCounter > CHECK_LOCK_INTERVAL) {
    solarBoxMan.debug->println("Zjistuji stav zamku " + String(currLockNum));
    checkLockState = false;
    lockStateCounter = 0;
    _refreshLockState();
  }

  _listenButtonLoop();

}