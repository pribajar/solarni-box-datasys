void _initModbus() {
  Serial1.begin(19200);
}
void _processQRCode() {
  if (currLockNum >  0 && currLockNum <= MAX_LOCK_NUM) {
    _refreshLockState();
  }
}

void _processSerialQRData() {
  //memset(qrCode,0,sizeof(qrCode));
  if (bcSerial.available()) {
    idleCounter = 0;
    rcCounter = 0;
    //serialReadStart = true;
    int ix = 0;
    while (bcSerial.available()) {
      byte bite = bcSerial.read();
      if (bite != 0xff) {
        mySerial.println(char(bite));
        qrCode[ix++] = char(bite);
      }
    }
    if (ix > 0) {
      _qrProgressText(_getTextRef20(String("Nacitani dat...")));
      currLockNum = 0;
      _lockNumRequest();
    }
  }
}


uint8_t _stateOfLock() {
  if (currLockNum > 0) {
    uint8_t addr = ((currLockNum - 1) / 8) + 1;
    uint8_t lockNum = ((currLockNum - 1) % 8) + 1;
    return _modBusRead(addr, lockNum);
  }
}

void _openLock() {
  if (currLockNum > 0) {
    uint8_t addr = ((currLockNum - 1) / 8) + 1;
    uint8_t lockNum = ((currLockNum - 1) % 8) + 1;
    _modBusWrite(addr, REG_OPEN_LOCK, lockNum);
  }
}

uint8_t _modBusRead(uint8_t addr, uint8_t reg) {
  node.begin(addr, Serial1);
  node.clearResponseBuffer();
  uint8_t result = node.readHoldingRegisters(reg, 1);
  if (result == node.ku8MBResponseTimedOut) return 0xff;
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0);
  }
  return 0xff;
}

void _modBusWrite(uint8_t addr, uint8_t reg, uint8_t value) {
  node.begin(addr, Serial1);
  node.clearResponseBuffer();
  node.writeSingleRegister(reg, value);
}
