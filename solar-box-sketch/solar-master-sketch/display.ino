
//Declaration of components
NexText tReadCode = NexText(2, 1, "tReadCode");
NexText tLockState = NexText(5, 3, "tLockState");
NexText tGPRS = NexText(4, 3, "tGPRS");
NexText tTemp = NexText(4, 7, "tTemp");
NexText tVolt = NexText(4, 5, "tVolt");
NexText tHeat = NexText(4, 9, "tHeat");
NexText tXAxis = NexText(4, 11, "tXAxis");
NexText tYAxis = NexText(4, 13, "tYAxis");
NexText tZAxis = NexText(4, 15, "tZAxis");
NexText tLockNum = NexText(3, 13, "tLockNum");
NexButton bQRcode = NexButton(1, 1, "bQRcode");
NexButton bMonitor = NexButton(1, 2, "bMonitor");
NexButton bOpenLock = NexButton(5, 2, "bOpenLock");
NexButton bOpenLockMan = NexButton(3, 11, "bOpenLockMan");
NexButton bBackToQRcode2 = NexButton(3, 14, "bBackToQRcode2");
NexButton bBackToQRcode = NexButton(5, 1, "bBackToQRcode");
NexButton bBackToMain = NexButton(2, 3, "bBackToMain");

/******Declaration of Nextion-Callbacks********/

NexTouch *nex_listen_list[] = {
  &bQRcode,
  &bMonitor,
  &bOpenLock,
  &bOpenLockMan,
  &bBackToQRcode,
  &bBackToQRcode2,
  &bBackToMain,
  NULL
};

void bQRcodePushCallback(void *ptr) {
  _switchBCReader(1);
  idleCounter = 0;
};

void bMonitorPushCallback(void *ptr) {
  if (_isGprsConnected()) {
    tGPRS.setText("Pripojeno");
  } else {
    tGPRS.setText("Nepripojeno");
  }
  tTemp.setText(_getBattTemp());
  tVolt.setText(_getBattVoltage());
  tHeat.setText(_getBattHeating());
  Vector vec = _getAccData();
  tXAxis.setText(_convertStringToChar8(String(vec.XAxis)));
  tYAxis.setText(_convertStringToChar8(String(vec.YAxis)));
  tZAxis.setText(_convertStringToChar8(String(vec.ZAxis)));
  idleCounter = 0;
};

void bOpenLockPushCallback(void *ptr) {
  _openLock();
  checkLockState = true;
  idleCounter = 0;
};

void bBackToQRcodePushCallback(void *ptr) {
  _goBackToQRcode();
};


void bBackToMainPushCallback(void *ptr) {
  _switchBCReader(0);
  idleCounter = 0;
};

void bOpenLockManPushCallback(void *ptr) {
  char chNum[3] = {0, 0, 0};
  tLockNum.getText(chNum, 2);
  uint8_t num =  atoi(chNum);
  tLockNum.setText("");
  if (num > 0 && num <= MAX_LOCK_NUM) {
    mySerial.println("Odemykani zamku " + String(num));
    currLockNum = num;
    _openLock();
  }
  idleCounter = 0;
}

void _attachDisplayEventMethods() {
    bQRcode.attachPush(bQRcodePushCallback);
    bMonitor.attachPush(bMonitorPushCallback);
    bOpenLock.attachPush(bOpenLockPushCallback);
    bOpenLockMan.attachPush(bOpenLockManPushCallback);
    bBackToQRcode.attachPush(bBackToQRcodePushCallback);
    bBackToQRcode2.attachPush(bBackToQRcodePushCallback);
    bBackToMain.attachPush(bBackToMainPushCallback);
}

void _qrProgressText(char * text) {
  tReadCode.setText(text);
}

void _refreshLockState() {
  String text = "Schranka cislo " + String(currLockNum) + " je " + _checkLockState();
  tLockState.setText(text.c_str());
}

String _checkLockState() {
  String res = "Error";
  switch (_stateOfLock()) {
    case 1:
      {
        res = "zamcena";
        _visible("bOpenLock", 1);
        break;
      }
    case 0:
      {
        res = "odemcena";
        _visible("bOpenLock", 0);
        break;
      }
    case 0xff:
      {
        res = "nedostupna";
        _visible("bOpenLock", 0);
        break;
      }
  }
  return res;
}

void _changePage(int pageId) {
  Serial.print("page " + String(pageId));
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}

void _visible(String comp, byte state) {
  Serial.print("vis " +  comp + "," + String(state));
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);  
}

void _goBackToQRcode() {
  _switchBCReader(1);
  tReadCode.setText("Nactete carovy kod");
  idleCounter = 0;
}

void _listenButtonLoop() {
  nexLoop(nex_listen_list);
}
