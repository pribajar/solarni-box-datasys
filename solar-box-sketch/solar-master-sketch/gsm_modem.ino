
void sendAT(String cmd = "") {
  if (cmd == "") {
    Serial3.println();
  } else {
    if (cmd == "0x1a") {
      Serial3.write(0x1a);
    } else {
      Serial3.println(cmd);
    }
  }
}

bool _isGprsConnected() {
  sendAT("AT+CGATT?"); 
  if (waitResponse("+CGATT: 1") != 1) { return false; }
  return true;
}

uint8_t waitResponse(uint16_t timeout, String& data, String OK_Resp = "X") {
  uint32_t startMillis = millis();
  do {
    while (Serial3.available()) {
      int8_t a = Serial3.read();
      if (a <= 0) continue;
      data += static_cast<char>(a);
      if (OK_Resp != "X" && (data.endsWith(OK_Resp) || data.indexOf(OK_Resp) > 0)) {
        return 1;
      }
    }
  } while ((millis() - startMillis) < timeout);
  return 0;
}

uint8_t waitResponse(String OK_Resp) {
  String data;
  return waitResponse(1000, data, OK_Resp);
}

uint8_t waitResponse() {
  String data;
  return waitResponse(1000, data, "OK");
}

void _sendData() {
  mySerial.println("Send data");
  if (_isGprsConnected()) {
    mySerial.println("Read diagnostic data");
    String volt = _getBattVoltage();
    delay(10);
    String temp = _getBattTemp();
    delay(10);
    String heat = _getBattHeating();
    Vector vec = _getAccData();
    char xAxis[8];
    memset(xAxis,0,sizeof(xAxis));
    String(vec.XAxis).toCharArray(xAxis, sizeof(xAxis));
    
    char yAxis[8];
    memset(yAxis,0,sizeof(yAxis));
    String(vec.YAxis).toCharArray(yAxis, sizeof(yAxis));
    
    char zAxis[8];
    memset(zAxis,0,sizeof(zAxis));
    String(vec.ZAxis).toCharArray(zAxis, sizeof(zAxis));
    
    String data = volt + ";" + temp + ";" + heat + ";" + xAxis + ";" + yAxis + ";" + zAxis;
    mySerial.println(data);
    sendAT("AT+CHTTPACT=\"" + server + "\"," + String(port));
    String response;
    String id = qrCode;
    if (waitResponse(2000, response, "REQUEST") == 1) {
      //mySerial.println(response);
      sendAT("POST http://" + server + postResource + " HTTP/1.1");
      sendAT("Host: " + server);
      sendAT("User-Agent: Box stock");
      sendAT("Content-Type:" + contentType);
      sendAT("Content-Length: " + String(data.length()));
      sendAT();
      sendAT(data);
      sendAT("0x1a");
      sendAT();
      response = "";
      waitResponse(2000, response);
    } else {
      mySerial.println("Send data error");
    }
  } else {
    mySerial.println("No data connection");
  }
}

void _lockNumRequest() {
  sendAT("AT+CHTTPACT=\"" + server + "\"," + String(port));
  String response;
  String id = qrCode;
  if (waitResponse(3000, response, "REQUEST") == 1) {
    //mySerial.println(response);
    sendAT("GET http://" + server + getResource + id + " HTTP/1.1");
    sendAT("Host: " + server);
    sendAT("User-Agent: Box stock");
    sendAT("Content-Length: 0");
    sendAT();
    sendAT("0x1a");
    sendAT();
    response = "";
    waitResponse(2000, response);
    int ix = 0;
    char num[3] = {0, 0, 0};
    for (int i = 0; i < response.length(); i++) {
      if (ix < 17 && response[i] == '\n') {
        ix++;
      }
      if (ix == 17) {
        num[0] = response[i + 1];
        num[1] = response[i + 2];
        ix++;
        break;
      }
    }
    mySerial.println(num);
    uint8_t iNum = atoi(num);
    if (iNum == 99) {
      _switchBCReader(0);
      _changePage(3);
    } else {
      if (iNum > 0 && iNum <= MAX_LOCK_NUM) {
        currLockNum = iNum;
        _changePage(5);
        _switchBCReader(0);
        _switchDisplay(0);
        _processQRCode();
      } else {
        _qrProgressText(_getTextRef20(String("Chyba nacitani dat")));
      }
    }
    mySerial.println(response);
  } else {
    mySerial.println("Error");
    _qrProgressText(_getTextRef20(String("Chyba pripojeni")));
  }

}
