void _getAllLockBoardsState() {
  for (uint8_t i = 15; i<16; i++) {
    if (!userMode)
      _getLockBoardState(i);
  }
}

void _getLockBoardState(uint8_t addr) {
    mySerial.println("Get data from board " + String(addr));
    node.begin(addr, Serial1);
    node.clearResponseBuffer();
    uint8_t result = 0;
    result = node.readHoldingRegisters(101,1);
    if (result == node.ku8MBResponseTimedOut) return;
    if (result == node.ku8MBSuccess)
      mySerial.println(node.getResponseBuffer(0));
    delay(100);
    result = node.readHoldingRegisters(102,1);
    if (result == node.ku8MBSuccess)
      mySerial.println(node.getResponseBuffer(0));
    delay(100);
    result = node.readHoldingRegisters(103,1);
    if (result == node.ku8MBSuccess)
      mySerial.println(node.getResponseBuffer(0));
    delay(100);
    result = node.readHoldingRegisters(104,1);
    if (result == node.ku8MBSuccess)
      mySerial.println(node.getResponseBuffer(0));        
    delay(100);
    node.writeSingleRegister(100,1);
    mySerial.println("Done");  
}
