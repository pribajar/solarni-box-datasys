#include <ModbusMaster.h>
#include <DS3231.h>
#include <Wire.h>
#include <Nextion.h>
#include <SoftwareSerial.h>
#include <avr/wdt.h>

#define LOCKBOARDS_POWER_PIN 4
#define LOCKS_POWER_PIN 37

#define LED_RED_PIN 13
#define LED_GREEN_PIN A4
#define LED_YELLOW_PIN A2
#define LED_BLUE_PIN A3
#define BC_READER_POWER_PIN A1
#define DISPLAY_POWER_PIN A12
#define USER_INT_PIN 2
#define GSM_MODEM_POWER_PIN 25
#define GSM_MODEM_PWRKEY_PIN 6
#define GSM_MODEM_SLEEP_PIN 5
#define SW_SERIAL_RX_PIN 11

#define TIMER_PRELOAD 63535 //1ms
#define IDLE_TIME 60000  //ms
#define CHECK_LOCK_INTERVAL 300 //ms
#define KEEP_ALIVE_INTERVAL 1000 //ms

#define MAX_GPRS_ATTEMPTS 2  //pocet pokusu o pripojeni k siti
#define MAX_LOCK_NUM 64     //maximalni pocet zamku

#define PIC_I2C_ADDR 0x04

//MASTER PIC I2C REGISTERS
#define REG_USER_MODE 100
#define REG_BATT_TEMP_LOW 101
#define REG_BATT_TEMP_HIGH 102
#define REG_POWER_OFF 103
#define REG_HEATING 104
#define REG_BATT_VOLT_LOW 105
#define REG_BATT_VOLT_HIGH 106
#define REG_KEEP_ALIVE 107
#define REG_ACC_X_LOW 108
#define REG_ACC_X_HIGH 109
#define REG_ACC_Y_LOW 110
#define REG_ACC_Y_HIGH 111
#define REG_ACC_Z_LOW 112
#define REG_ACC_Z_HIGH 113

//LOCK PIC MODBUS REGISTERS
#define REG_OPEN_LOCK 100

//referencni hodnota 12V
#define REF_BATT_VOLTAGE 550.0

struct Vector
{
    float XAxis;
    float YAxis;
    float ZAxis;
};
bool serialReadStart, checkLockState, boardStarted = false;
uint16_t idleCounter, rcCounter, lockStateCounter, keepAliveCounter;
bool userMode = 0;
uint8_t currLockNum, gprsAttempts;
//String json = "CHECK_PARCEL_BARCODE;{\"CheckParcelBarcode\":{\"Barcode\":\"ET123456789SK\"}}";
char qrCode[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//Debug serial init
SoftwareSerial mySerial(A10, A9); // RX, TX

//Modem
// Your GPRS credentials, if any
const char apn[]      = "Internet";
//const char gprsUser[] = "";
//const char gprsPass[] = "";

// Server details
const String server   = "picon.i-development.cz";
const String getResource = "/Box/GetLockId?id=";
const String contentType ="text/plain";
const String postResource = "/Box/SaveBoxState";
const int  port       = 5000;

void userButton_ISR() {
  mySerial.println("User interrupt");
  _powerOffPer();
}
//BC reader
SoftwareSerial bcSerial(SW_SERIAL_RX_PIN, 2); // RX, TX

//Modbus init
ModbusMaster node;

DS3231 rtc;

void setup() {
  _onSetup();
  
}

ISR(TIMER1_OVF_vect)                    // interrupt service routine for overflow
{
  TCNT1 = TIMER_PRELOAD;                                // preload timer
  idleCounter++;
  keepAliveCounter++;
  if (checkLockState) {
    lockStateCounter++;
  }
  if (serialReadStart) {
    rcCounter++;
  }
}

void loop() {
  if (!boardStarted) {
    _init();
  }   
  
  if (idleCounter >= IDLE_TIME) {
    _powerOff();
  }

  bcSerial.listen();
  if (serialReadStart == false && bcSerial.available()) {
    rcCounter = 0;
    serialReadStart = true;
  }

  if (serialReadStart && rcCounter > 22) {
    _processSerialQRData();
    rcCounter = 0;
    serialReadStart = false;
    _processQRCode();
  }

  if (checkLockState && lockStateCounter > CHECK_LOCK_INTERVAL) {
    mySerial.println("Zjistuji stav zamku " + String(currLockNum));
    checkLockState = false;
    lockStateCounter = 0;
    _refreshLockState();
  }

  if (keepAliveCounter >= KEEP_ALIVE_INTERVAL) {
    _keepAlive();
  }
  _listenButtonLoop();

}
