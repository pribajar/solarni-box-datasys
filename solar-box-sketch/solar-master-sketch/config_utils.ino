void _digitalWriteD(uint8_t pin, uint8_t val) {
  uint8_t bit = 1 << pin;
  uint8_t oldSREG = SREG;
  cli();

  if (val == LOW) {
    PORTD &= ~bit;
  } else {
    PORTD |= bit;
  }
  SREG = oldSREG;
}

void _digitalWriteK(uint8_t pin, uint8_t val) {
  uint8_t bit = 1 << pin;
  uint8_t oldSREG = SREG;
  cli();

  if (val == LOW) {
    PORTK &= ~bit;
  } else {
    PORTK |= bit;
  }
  SREG = oldSREG;
}

void _pinModeK(uint8_t pin, uint8_t mode) {
  uint8_t bit = 1 << pin;
  if (mode == INPUT) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTK &= ~bit;
    SREG = oldSREG;
  } else if (mode == INPUT_PULLUP) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTK |= bit;
    SREG = oldSREG;
  } else {
    uint8_t oldSREG = SREG;
    cli();
    DDRD |= bit;
    SREG = oldSREG;
  }
}

void _pinModeD(uint8_t pin, uint8_t mode) {
  uint8_t bit = 1 << pin;
  if (mode == INPUT) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTD &= ~bit;
    SREG = oldSREG;
  } else if (mode == INPUT_PULLUP) {
    uint8_t oldSREG = SREG;
    cli();
    DDRD &= ~bit;
    PORTD |= bit;
    SREG = oldSREG;
  } else {
    uint8_t oldSREG = SREG;
    cli();
    DDRD |= bit;
    SREG = oldSREG;
  }
}

void _allLedsOff() {
  digitalWrite(LED_RED_PIN, 0);
  digitalWrite(LED_GREEN_PIN, 1);
  digitalWrite(LED_BLUE_PIN, 1);
  digitalWrite(LED_YELLOW_PIN, 1);
}

void _setTimer() {
  noInterrupts();                       // disable all interrupts

  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = TIMER_PRELOAD;                        // preload timer 1ms
  TCCR1B |= (1 << CS11);    // 8 prescaler
  TIMSK1 |= (1 << TOIE1);               // enable timer overflow interrupt ISR
  interrupts();                         // enable all interrupts
}

void _onSetup() {
  _setPins();
  _switchBCReader(0);
  _allLedsOff();
  _setTimer();
  _initModbus();
  Wire.begin();
  mySerial.begin(115200);
  attachInterrupt(digitalPinToInterrupt(USER_INT_PIN), userButton_ISR, RISING);

  mySerial.println("Start OK");


  //Serial3.begin(115200);
  //delay(50);
  //modem.init();
  //startGsmQueue = true;
  /*rtc.setYear(22);
    rtc.setMonth(1);
    rtc.setHour(13);
    rtc.setMinute(3);
    rtc.setSecond(1);*/
}

char * _convertStringToChar8(String value) {
  static char chRes[8];
  value.toCharArray(chRes,value.length()+1);
  return  chRes;
}

char * _getTextRef20(String text){
  static char chText[20];
  memset(chText,0,sizeof(chText));
  text.toCharArray(chText,sizeof(chText));
  return chText;
}

void _init() {
  if (_isUserMode() == 1) {
    _switchLockBoardsPower(1);
    _switchLocksPower(1);
  }
  delay(300);
  _digitalWriteD(GSM_MODEM_PWRKEY_PIN, 0);
  _digitalWriteD(GSM_MODEM_SLEEP_PIN, 0);
  delay(50);
  _switchModemPower(1);
  
  mySerial.println("Wait...");
  delay(10000);
  Serial3.begin(115200);  //GSM Modem serial init
  delay(6000);
  uint32_t startMillis = millis();
  while (!_isGprsConnected() && (millis()-startMillis) < 5000) {
    delay(1000);
  }

  if (_isUserMode() == 1) {
    //init display
    mySerial.println("User mode 1");
    bcSerial.begin(9600);  //ctecka
    Serial.begin(9600); //display
    nexInit();
    _attachDisplayEventMethods();
    _changePage(1);
  } else {
    mySerial.println("User mode 0");
    _sendData();
    delay(10000);
    _powerOff();
  }
  boardStarted = true;
}

void _setPins() {
  pinMode(LED_RED_PIN, OUTPUT);
  pinMode(LED_GREEN_PIN, OUTPUT);
  pinMode(LED_YELLOW_PIN, OUTPUT);
  pinMode(LED_BLUE_PIN, OUTPUT);
  _pinModeD(LOCKBOARDS_POWER_PIN, OUTPUT);
  pinMode(LOCKS_POWER_PIN, OUTPUT);
  pinMode(DISPLAY_POWER_PIN, OUTPUT);
  pinMode(BC_READER_POWER_PIN, OUTPUT);
  pinMode(GSM_MODEM_POWER_PIN, OUTPUT);
  pinMode(USER_INT_PIN, INPUT);
  _pinModeD(GSM_MODEM_PWRKEY_PIN, OUTPUT);
  _pinModeD(GSM_MODEM_SLEEP_PIN, OUTPUT);
}
