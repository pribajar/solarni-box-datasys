char * _getBattTemp() {
  uint16_t raw_temp = _ReadI2C16(REG_BATT_TEMP_LOW);
  static char temp[7] = "000.00";
      if(raw_temp & 0x8000)  // if the temperature is negative
      {
        temp[0] = '-';             // put minus sign (-)
        raw_temp = (~raw_temp) + 1;  // change temperature value to positive form
      }
 
      else
      {
        if((raw_temp >> 4) >= 100)  // if the temperature >= 100 °C
          temp[0] = '1';            // put 1 of hundreds
        else                        // otherwise
          temp[0] = ' ';            // put space ' '
      }
 
      // put the first two digits ( for tens and ones)
      temp[1] = ( (raw_temp >> 4) / 10 ) % 10 + '0';  // put tens digit
      temp[2] =   (raw_temp >> 4)        % 10 + '0';  // put ones digit
 
      // put the 4 fraction digits (digits after the point)
      // why 625?  because we're working with 12-bit resolution (default resolution)
      temp[4] = ( (raw_temp & 0x0F) * 625) / 1000 + '0';          // put thousands digit
      temp[5] = (((raw_temp & 0x0F) * 625) / 100 ) % 10 + '0';    // put hundreds digit
      //temp[6] = (((raw_temp & 0x0F) * 625) / 10 )  % 10 + '0';    // put tens digit
      //temp[7] = ( (raw_temp & 0x0F) * 625) % 10 + '0';            // put ones digit
  return temp; 
}

char * _getBattVoltage() {
  uint16_t raw_volt = _ReadI2C16(REG_BATT_VOLT_LOW);
  float result = 12.0 * (raw_volt / REF_BATT_VOLTAGE);
  static char chRes[8];
  memset(chRes,0,sizeof(chRes));
  String(result).toCharArray(chRes,sizeof(chRes));
  mySerial.println(chRes);
  return chRes;
}

Vector _getAccData() {
  int16_t raw_Xacc = (int16_t)_ReadI2C16(REG_ACC_X_LOW);
  Vector vec;
  vec.XAxis = _getAngle((int16_t)_ReadI2C16(REG_ACC_X_LOW));
  vec.YAxis = _getAngle((int16_t)_ReadI2C16(REG_ACC_Y_LOW));
  vec.ZAxis = _getAngle((int16_t)_ReadI2C16(REG_ACC_Z_LOW));
  mySerial.println(vec.XAxis);
  mySerial.println(vec.YAxis);
  mySerial.println(vec.ZAxis);
  return vec;
}

float _getAngle(int16_t value) {
    float sinVal = value * .000061f;
  if (abs(sinVal) > 1) sinVal = 1;
  return (asin(sinVal) * 180./3.14);
}

char * _getBattHeating() {
    uint8_t raw_heat = 0;
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(REG_HEATING);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    raw_heat = Wire.read();
  }  
  String sRes = "Vypnuto";
  if (raw_heat > 0) {
    sRes = "Zapnuto";
  }
  static char chRes[8];
  memset(chRes,0,sizeof(chRes));
  sRes.toCharArray(chRes,sizeof(chRes));
  return chRes;
}

uint16_t _ReadI2C16(uint8_t regAddr){
  uint16_t raw = 0;
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(regAddr);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    raw = Wire.read();
  }
  delay(10);
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(regAddr+1);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    uint16_t hRaw = Wire.read();
    raw |= hRaw << 8;
  }
  return raw;
}

//1 - pokud byl spusteny tlacitkem, 0 - automaticky
uint8_t _isUserMode(){
  uint8_t res = -1;
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(REG_USER_MODE);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    return Wire.read();
  }
  return -1;
}

void _powerOffPer() {
  _switchModemPower(0);
  _switchLockBoardsPower(0);
  _switchLocksPower(0);
  _switchBCReader(0);
  _switchDisplay(0);
}

void _keepAlive(){
  keepAliveCounter = 0;
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(REG_KEEP_ALIVE);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    Wire.read();
  }
}

//vypne napajeni Atmelu
void _powerOff(){
  _powerOffPer();
  Wire.beginTransmission(PIC_I2C_ADDR);
  Wire.write(REG_POWER_OFF);
  Wire.endTransmission();
  Wire.requestFrom(PIC_I2C_ADDR,1);
  if (Wire.available() == 1) {
    Wire.read();
  }
  delay(50);
}

void _switchModemPower(bool value) {
  digitalWrite(GSM_MODEM_POWER_PIN, value);
}

void _switchLockBoardsPower(bool value){
  _digitalWriteD(LOCKBOARDS_POWER_PIN, value);
}

void _switchLocksPower(bool value){
  digitalWrite(LOCKS_POWER_PIN, value);
}

void _switchBCReader(bool value){
  digitalWrite(BC_READER_POWER_PIN, value);
  _switchDisplay(value);
}

void _switchDisplay(bool value){
  digitalWrite(DISPLAY_POWER_PIN, value);
}
